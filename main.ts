
interface City {
    zip: number;
    name: string;
}

function printCityByName(a: City[]) {
    return a.slice(1);
}

let x: any[] = [20001, "Washington"];

printCityByName(x);

let hex: number = 0xfff;
enum Color { White = 1, Black = 2, Green = 3, Yellow = 4, Blue = 5, Red = 6 }
let c: Color = Color.Green;

class Traffic {
    vehicles: number;
    constructor(theNumberOfVehicles: number) { this.vehicles = theNumberOfVehicles; }
    run() { console.log(this.vehicles); };
}

class North extends Traffic {
    constructor(vehicles: number) {
        super(vehicles);
    }
    run() {
        super.run();
    }
}

class South extends Traffic {
    constructor(vehicles: number) {
        super(vehicles);
    }
    run() {
        super.run();
    }
}

let ford = new North(30200);
let nissan = new South(12040); 

ford.run();
nissan.run();

function extend<T, U>(first: T, second: U):
T & U {
    let result = <T & U>{};
    for (let id in first ){
        (<any>result)[id] = (<any>first)[id]
    }
    for (let id in second) {
        if (!result.hasOwnProperty(id)){
            (<any>result)[id] = (<any> second)[id];
        }
    }
    return result;
}

class StreetByName {
    constructor(public name : string) { }
}

interface Loggable {
    log(): void;
}

class ConsoleLogger implements Loggable {
    log() {
        // Empty method 
    }
} 

var avenue = extend(new StreetByName("Independence Avenue"), new ConsoleLogger());
var n = avenue.name;
avenue.log();