"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
function printCityByName(a) {
    return a.slice(1);
}
var x = [20001, "Washington"];
printCityByName(x);
var hex = 0xfff;
var Color;
(function (Color) {
    Color[Color["White"] = 1] = "White";
    Color[Color["Black"] = 2] = "Black";
    Color[Color["Green"] = 3] = "Green";
    Color[Color["Yellow"] = 4] = "Yellow";
    Color[Color["Blue"] = 5] = "Blue";
    Color[Color["Red"] = 6] = "Red";
})(Color || (Color = {}));
var c = Color.Green;
var Traffic = /** @class */ (function () {
    function Traffic(theNumberOfVehicles) {
        this.vehicles = theNumberOfVehicles;
    }
    Traffic.prototype.run = function () { console.log(this.vehicles); };
    ;
    return Traffic;
}());
var North = /** @class */ (function (_super) {
    __extends(North, _super);
    function North(vehicles) {
        return _super.call(this, vehicles) || this;
    }
    North.prototype.run = function () {
        _super.prototype.run.call(this);
    };
    return North;
}(Traffic));
var South = /** @class */ (function (_super) {
    __extends(South, _super);
    function South(vehicles) {
        return _super.call(this, vehicles) || this;
    }
    South.prototype.run = function () {
        _super.prototype.run.call(this);
    };
    return South;
}(Traffic));
var ford = new North(30200);
var nissan = new South(12040);
ford.run();
nissan.run();
function extend(first, second) {
    var result = {};
    for (var id in first) {
        result[id] = first[id];
    }
    for (var id in second) {
        if (!result.hasOwnProperty(id)) {
            result[id] = second[id];
        }
    }
    return result;
}
var StreetByName = /** @class */ (function () {
    function StreetByName(name) {
        this.name = name;
    }
    return StreetByName;
}());
var ConsoleLogger = /** @class */ (function () {
    function ConsoleLogger() {
    }
    ConsoleLogger.prototype.log = function () {
        // Empty method 
    };
    return ConsoleLogger;
}());
var avenue = extend(new StreetByName("Independence Avenue"), new ConsoleLogger());
var n = avenue.name;
avenue.log();
